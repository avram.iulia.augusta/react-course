import React from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';


class App extends React.Component {

    state = { lat: null, errorMsg: '' };

    componentDidMount() {
        // good place to do data loading
        window.navigator.geolocation.getCurrentPosition(
            (position) => this.setState({ lat: position.coords.latitude }),
            (err) => this.setState({ errorMsg: err.message })
        );
    }

    renderContent() {
        // avoid conditionals in the render method
        if (this.state.errorMsg && !this.state.lat) {
            return <div>Error: {this.state.errorMsg}</div>
        } else if (this.state.lat && !this.state.errorMsg) {
            return <SeasonDisplay lat={this.state.lat}/>
        } else {
            return <Spinner />
        }
    }

    render () {
        return (
            <div className="content">
                {this.renderContent()}
            </div>
        );
    }
}

ReactDOM.render(
    <App />,
    document.querySelector('#root')
)