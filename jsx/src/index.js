// import the React and ReactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';

// create a React component
const App = () => {
    const buttonInfo = { text: "Click me!" };
    const labelText = "Enter text:";

    const style = { backgroundColor: 'blue', color: 'white'};

    return (
    <div>
        <label className="label" htmlFor="name">
            {labelText}
        </label>
        <input id="name" type="text" />
        <button style={style}>
            {buttonInfo.text}
        </button>
    </div>
    );
};

// take the react component and show it on the screen
ReactDOM.render(
    <App />,
    document.querySelector('#root')
)
