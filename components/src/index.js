import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {

    return (
        <div className="ui container comments"> 
            <br/>
            <ApprovalCard>
                <CommentDetail 
                    author="Sam"
                    avatar={faker.image.avatar()} 
                    timeAgo="Todat at 6 PM" 
                    text="Hey girls!" />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail 
                    author="Alex" 
                    avatar={faker.image.avatar()}
                    timeAgo="Today at 7:45 PM" 
                    text="Heyaa" />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail 
                    author="Clover" 
                    avatar={faker.image.avatar()} 
                    timeAgo="Today at 7:59 PM" 
                    text="xx"/>
            </ApprovalCard>
        </div>
    );

};

ReactDOM.render(<App/>, document.querySelector('#root'));